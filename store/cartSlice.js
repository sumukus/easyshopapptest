import { createSlice } from "@reduxjs/toolkit";

export const cartSlice = createSlice({
  name: "cart",
  initialState: {
    value: [],
    shippingAddressStatus: false,
    paymentDetailStatus: false,
    shippingAddress: {},
  },
  reducers: {
    addToCart: (state, action) => {
      state.value.push(action.payload);
    },

    clearCart: (state) => {
      state.value = [];
    },

    removeFromCart: (state, action) => {
      state.value = state.value.filter(
        (cartItem) => cartItem.cartOrderId != action.payload.cartOrderId
      );
    },
    setShippingAddressStatus: (state, action) => {
      state.shippingAddressStatus = action.payload;
    },

    setPaymentDetailStatus: (state, action) => {
      state.paymentDetailStatus = action.payload;
    },

    setShippingAddress: (state, action) => {
      state.shippingAddress = action.payload;
    },
  },
});

export const {
  addToCart,
  clearCart,
  removeFromCart,
  setShippingAddressStatus,
  setPaymentDetailStatus,
  setShippingAddress,
} = cartSlice.actions;

export default cartSlice.reducer;
