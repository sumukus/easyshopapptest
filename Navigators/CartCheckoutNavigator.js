import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import Checkout from "../components/Cart/Checkout";
import Payment from "../components/Cart/Payment";
import Confirm from "../components/Cart/Confirm";
import { useSelector } from "react-redux";

const Tab = createMaterialTopTabNavigator();

function CartCheckoutNavigator() {
  const { shippingAddressStatus, paymentDetailStatus } = useSelector(
    (state) => state.cart
  );
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="ShippingAddress"
        component={Checkout}
        options={{
          title: "Shipping Address",
        }}
      />
      <Tab.Screen
        name="Payment"
        component={Payment}
        options={{
          title: "Payment Details",
        }}
        listeners={{
          tabPress: (event) => {
            if (shippingAddressStatus == false) {
              event.preventDefault();
              alert("Please complete the shipping address before payment");
            }
          },
        }}
      />
      <Tab.Screen
        name="Confirm"
        component={Confirm}
        listeners={{
          tabPress: (event) => {
            if (paymentDetailStatus == false) {
              event.preventDefault();
              alert("Please complete the payment to place order");
            }
          },
        }}
      />
    </Tab.Navigator>
  );
}

export default CartCheckoutNavigator;
