import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Cart from "../screens/Cart/Cart";
import CartCheckoutNavigator from "./CartCheckoutNavigator";

const Stack = createNativeStackNavigator();

function CartNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: "#e3e8ee",
        },
      }}
    >
      <Stack.Screen name="Cart" component={Cart} />
      <Stack.Screen name="Checkout" component={CartCheckoutNavigator} />
    </Stack.Navigator>
  );
}
export default CartNavigator;
