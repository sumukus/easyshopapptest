import { createNativeStackNavigator } from "@react-navigation/native-stack";
import ProductContainer from "../screens/Products/ProductContainer";
import SingleProduct from "../components/Products/SingleProduct";

const Stack = createNativeStackNavigator();

function ProductNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        headerStyle: {
          backgroundColor: "#e3e8ee",
        },
      }}
    >
      <Stack.Screen name="ProductContainer" component={ProductContainer} />
      <Stack.Screen
        name="ProductDetails"
        component={SingleProduct}
        options={{
          title: "Product Details",
          headerShown: true,
        }}
      />
    </Stack.Navigator>
  );
}

export default ProductNavigator;
