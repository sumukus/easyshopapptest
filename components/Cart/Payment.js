import { useState, useEffect } from "react";
import { View, Text, StyleSheet, Button } from "react-native";
import PaymentType from "./PaymentType";
import DropDownItemPicker from "./DropDownItemPicker";
import { useDispatch } from "react-redux";
import { setPaymentDetailStatus } from "../../store/cartSlice";
import { useNavigation } from "@react-navigation/native";

function Payment() {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const payments = ["Cash On Delivery", "Bank Transfer", "Card Payment"];
  //We defined the cardTypes as array og objects
  //Because DropDownItemPicker components expects data
  //As an array of object with name property
  const cardPaymentTypes = [
    { name: "CreditCard" },
    { name: "MasterCard" },
    { name: "Wallet" },
    { name: "Other" },
  ];

  //Make use of cardTypes
  //If you want to save it
  const [cardTypes, setCardTypes] = useState(null);

  //Make use of paymentType
  //If you want to save it
  const [paymentType, setPaymentType] = useState(payments[0]);

  console.log(`Payment Method: ${paymentType}, Card Types: ${cardTypes}`);

  function handleNext() {
    dispatch(setPaymentDetailStatus(true));
    navigation.navigate("Confirm");
  }

  return (
    <View style={styles.rootContainer}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>Select the Payment Method</Text>
      </View>
      {payments.map((payment) => (
        <PaymentType
          key={payment}
          paymentType={paymentType}
          setPaymentType={setPaymentType}
        >
          {payment}
        </PaymentType>
      ))}
      {paymentType === payments[2] ? (
        <DropDownItemPicker
          data={cardPaymentTypes}
          value={cardTypes}
          setValue={setCardTypes}
          placeholder="Select Card Types"
          listMode="FLATLIST"
          searchable={false}
        />
      ) : null}
      <View style={styles.buttonNext}>
        <Button title="Next" onPress={handleNext} />
      </View>
    </View>
  );
}

export default Payment;

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    alignItems: "center",
  },
  titleContainer: {
    width: "100%",
    backgroundColor: "blue",
    paddingVertical: 10,
    paddingLeft: 10,
  },
  title: {
    color: "white",
    fontSize: 16,
    fontWeight: "bold",
  },
  buttonNext: {
    paddingTop: 10,
  },
});
