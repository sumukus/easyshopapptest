import { View, Text, StyleSheet } from "react-native";
import { useSelector } from "react-redux";

function CartIconNumber() {
  const cartItems = useSelector((state) => state.cart.value);
  return (
    <>
      {cartItems.length > 0 ? (
        <View style={styles.rootContainer}>
          <Text style={styles.numberItem}>{cartItems.length}</Text>
        </View>
      ) : null}
    </>
  );
}

export default CartIconNumber;

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    width: 25,
    backgroundColor: "red",
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 2,
    paddingHorizontal: 4,
    borderRadius: 20,
    position: "absolute",
    top: 1,
    right: 25,
  },
  numberItem: {
    color: "white",
    fontWeight: "bold",
  },
});
