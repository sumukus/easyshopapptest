import { TextInput, StyleSheet } from "react-native";

function Input({ ...config }) {
  return <TextInput style={styles.InputText} {...config} />;
}

export default Input;

const styles = StyleSheet.create({
  InputText: {
    borderWidth: 1,
    borderColor: "orange",
    backgroundColor: "white",
    marginHorizontal: 20,
    marginVertical: 5,
    paddingVertical: 10,
    paddingLeft: 5,
  },
});
