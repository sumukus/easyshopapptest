import { Text, View, Image, StyleSheet, Button } from "react-native";
import { useDispatch } from "react-redux";
import { removeFromCart } from "../../store/cartSlice";

function SingleItem({ cartItem }) {
  const dispatch = useDispatch();
  return (
    <View style={styles.rootContainer}>
      <View style={styles.imageContainer}>
        <Image
          style={styles.image}
          source={{
            uri: cartItem.product.image
              ? cartItem.product.image
              : "https://cdn.pixabay.com/photo/2023/03/22/21/57/animal-7870631_1280.jpg",
          }}
        />
      </View>
      <View style={styles.productDetails}>
        <Text style={styles.productName}>{cartItem.product.name}</Text>
        <Text style={styles.productPrice}>
          Nu.{cartItem.product.price.toFixed(2)}
        </Text>
        <Button
          title="Delete"
          color="red"
          onPress={() => dispatch(removeFromCart(cartItem))}
        />
      </View>
    </View>
  );
}

export default SingleItem;

const styles = StyleSheet.create({
  rootContainer: {
    paddingHorizontal: 10,
    flexDirection: "row",
    marginBottom: 10,
    justifyContent: "center",
  },
  image: {
    width: "100%",
    height: "100%",
  },
  imageContainer: {
    height: 60,
    width: 60,
    flex: 1,
    borderRadius: 10,
    overflow: "hidden",
  },
  productDetails: {
    marginLeft: 10,
    borderBottomWidth: 1,
    borderColor: "grey",
    flexDirection: "row",
    flex: 3,
    justifyContent: "space-between",
    alignItems: "center",
  },
  productName: {
    fontSize: 14,
    fontWeight: "bold",
  },

  productPrice: {
    fontSize: 14,
    fontWeight: "bold",
    color: "orange",
  },
});
