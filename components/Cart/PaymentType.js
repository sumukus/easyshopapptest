import { View, Text, StyleSheet } from "react-native";
import { Ionicons } from "@expo/vector-icons";

function PaymentType({ children, paymentType, setPaymentType }) {
  return (
    <View style={styles.rootContainer}>
      <View style={styles.labelContainer}>
        <Text style={styles.labelText}>{children}</Text>
      </View>
      <View style={styles.radioButton}>
        <Ionicons
          name={
            paymentType == children ? "radio-button-on" : "radio-button-off"
          }
          size={24}
          color="blue"
          onPress={() => setPaymentType(children)}
        />
      </View>
    </View>
  );
}

export default PaymentType;

const styles = StyleSheet.create({
  rootContainer: {
    flexDirection: "row",
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#e0d6d6",
    marginHorizontal: 10,
  },
  labelContainer: {
    flex: 1,
    paddingLeft: 10,
  },
  labelText: {
    fontSize: 16,
  },
  radioButton: {
    flex: 1,
    alignItems: "flex-end",
    paddingRight: 20,
  },
  pressed: {
    color: "blue",
  },
});
