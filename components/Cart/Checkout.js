import { useState, useEffect } from "react";
import { View, StyleSheet, ScrollView, Button } from "react-native";
import Input from "./Input";
import DropDownItemPicker from "./DropDownItemPicker";
import { useDispatch } from "react-redux";
import {
  setShippingAddressStatus,
  setShippingAddress,
} from "../../store/cartSlice";
import { useNavigation } from "@react-navigation/native";

const countriesData = require("../../assets/data/countries.json");

function Checkout() {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const [phoneNumber, setPhoneNumber] = useState();
  const [address1, setAddress1] = useState();
  const [address2, setAddress2] = useState();
  const [city, setCity] = useState();
  const [zipCode, setZipCode] = useState();
  const [country, setCountry] = useState(null);

  //we are not doing anything with the entered details
  //In real case you would store it in backend
  //Since  backend is not connected as of now
  // We didnt do anything with the entered value
  //But it is accessable through the state variables defined above
  //In case if you want to paly around
  function handleNext() {
    dispatch(setShippingAddressStatus(true));
    dispatch(
      setShippingAddress({
        phoneNumber: phoneNumber,
        address1: address1,
        address2: address2,
        city: city,
        zipCode: zipCode,
        country: country,
      })
    );
    navigation.navigate("Payment");
  }

  return (
    <View style={styles.rootContainer}>
      <ScrollView>
        <Input
          placeholder="Phone Number"
          value={phoneNumber}
          onChangeText={(enteredPhoneNumber) =>
            setPhoneNumber(enteredPhoneNumber)
          }
          keyboardType="phone-pad"
        />
        <Input
          placeholder="Shipping Address 1"
          value={address1}
          onChangeText={(enteredAddress1) => setAddress1(enteredAddress1)}
        />
        <Input
          placeholder="Shipping Address 2"
          value={address2}
          onChangeText={(enteredAddress2) => setAddress2(enteredAddress2)}
        />
        <Input
          placeholder="City"
          value={city}
          onChangeText={(enterdCity) => setCity(enterdCity)}
        />
        <Input
          placeholder="Zip Code"
          value={zipCode}
          onChangeText={(enteredZipCode) => setZipCode(enteredZipCode)}
          keyboardType="number-pad"
        />
        <DropDownItemPicker
          data={countriesData}
          value={country}
          setValue={setCountry}
          placeholder="Select the country"
          listMode="MODAL"
          searchable={true}
        />
        <View style={styles.button}>
          <Button title="Next" onPress={handleNext} />
        </View>
      </ScrollView>
    </View>
  );
}

export default Checkout;

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    paddingTop: 10,
  },
  button: {
    paddingTop: 10,
    alignItems: "center",
    justifyContent: "center",
  },
});
