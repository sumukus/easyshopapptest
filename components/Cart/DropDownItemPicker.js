import { useEffect, useState } from "react";
import { View, StyleSheet } from "react-native";
import DropDownPicker from "react-native-dropdown-picker";

function DropDownItemPicker({
  data,
  value,
  setValue,
  placeholder,
  listMode,
  searchable,
}) {
  const [open, setOpen] = useState(false);
  const [items, setItems] = useState();

  useEffect(() => {
    const newData = data.map((item) => ({
      label: item.name,
      value: item.name,
    }));
    setItems(newData);
  }, []);

  return (
    <View style={styles.rootContainer}>
      <DropDownPicker
        open={open}
        value={value}
        items={items}
        setOpen={setOpen}
        setValue={setValue}
        setItems={setItems}
        searchable={searchable}
        listMode={listMode}
        placeholder={placeholder}
        dropDownDirection="TOP"
        dropDownContainerStyle={{ backgroundColor: "#d5dfe7" }}
      />
    </View>
  );
}

export default DropDownItemPicker;

const styles = StyleSheet.create({
  rootContainer: {
    marginTop: 10,
    marginHorizontal: 20,
  },
});
