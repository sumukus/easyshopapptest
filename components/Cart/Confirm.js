import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  Button,
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import {
  setPaymentDetailStatus,
  setShippingAddressStatus,
  clearCart,
  setShippingAddress,
} from "../../store/cartSlice";
import { useNavigation } from "@react-navigation/native";

function Confirm() {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const shippingAddress = useSelector((state) => state.cart.shippingAddress);
  const cartItems = useSelector((state) => state.cart.value);

  let total = 0;
  cartItems.map((item) => (total += item.product.price));

  function handlePlaceOrder() {
    dispatch(clearCart());
    dispatch(setPaymentDetailStatus(false));
    dispatch(setShippingAddressStatus(false));
    dispatch(setShippingAddress({}));
    navigation.navigate("Cart");
  }

  return (
    <View style={styles.rootContainer}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>Order Confirmation</Text>
      </View>
      <ScrollView>
        <View style={styles.addressDetailsContainer}>
          <Text style={styles.subTitle}>Shipping Address Details:</Text>

          <View style={styles.details}>
            <Text>Phone Number:</Text>
            <Text>{shippingAddress.phoneNumber}</Text>
          </View>
          <View style={styles.details}>
            <Text>Address1:</Text>
            <Text>{shippingAddress.address1}</Text>
          </View>
          <View style={styles.details}>
            <Text>Address2:</Text>
            <Text>{shippingAddress.address2}</Text>
          </View>
          <View style={styles.details}>
            <Text>City:</Text>
            <Text>{shippingAddress.city}</Text>
          </View>
          <View style={styles.details}>
            <Text>ZipCode:</Text>
            <Text>{shippingAddress.zipCode}</Text>
          </View>
          <View style={styles.details}>
            <Text>Country:</Text>
            <Text>{shippingAddress.country}</Text>
          </View>
          <Text style={styles.subTitle}>Items:</Text>
          {cartItems.map((item) => (
            <View style={styles.poductConatiner} key={item.cartOrderId}>
              <View style={styles.imageContainer}>
                <Image
                  source={{
                    uri: item.product.image
                      ? item.product.image
                      : "https://cdn.pixabay.com/photo/2023/03/22/21/57/animal-7870631_1280.jpg",
                  }}
                  style={styles.image}
                />
              </View>
              <Text key={item.cartOrderId}>{item.product.name}</Text>
              <Text>Nu.{item.product.price.toFixed(2)}</Text>
            </View>
          ))}
          <View style={styles.totalContainer}>
            <Text style={styles.total}>Total</Text>
            <Text style={styles.totalPrice}>Nu.{total.toFixed(2)}</Text>
          </View>
        </View>
        <View style={styles.buttonPlaceOrder}>
          <Button title="Place Order" onPress={handlePlaceOrder} />
        </View>
      </ScrollView>
    </View>
  );
}

export default Confirm;

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
  },
  titleContainer: {
    width: "100%",
    backgroundColor: "blue",
    paddingVertical: 10,
  },
  title: {
    color: "white",
    fontSize: 16,
    textAlign: "center",
    fontWeight: "bold",
  },
  addressDetailsContainer: {
    paddingHorizontal: 10,
    marginVertical: 10,
    marginHorizontal: 15,
    // borderWidth: 1,
    // borderColor: "orange",
    paddingVertical: 15,
    backgroundColor: "#f9ec59",
    borderRadius: 20,
  },
  details: {
    flexDirection: "row",
    paddingVertical: 5,
  },
  subTitle: {
    textAlign: "center",
    paddingVertical: 10,
    fontWeight: "bold",
    fontSize: 16,
  },
  poductConatiner: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 5,
    borderBottomWidth: 1,
    borderBottomColor: "#ccd3db",
  },
  imageContainer: {
    width: 50,
    height: 25,
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    width: "100%",
    height: "100%",
  },
  buttonPlaceOrder: {
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 10,
  },
  totalContainer: {
    flexDirection: "row",
    paddingTop: 10,
    justifyContent: "space-between",
  },
  total: {
    fontSize: 16,
    fontWeight: "bold",
  },
  totalPrice: {
    color: "red",
    fontSize: 16,
    fontWeight: "bold",
  },
});
