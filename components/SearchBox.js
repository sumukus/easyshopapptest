import { View, StyleSheet, TextInput } from "react-native";
import { Ionicons } from "@expo/vector-icons";
function SearchBox({ onChangeText }) {
  return (
    <View style={styles.rootContainer}>
      <Ionicons style={styles.searchIcon} name="search" size={18} />
      <TextInput
        placeholder="Search"
        onChangeText={onChangeText}
        style={styles.textInput}
      />
    </View>
  );
}

export default SearchBox;

const styles = StyleSheet.create({
  rootContainer: {
    flexDirection: "row",
    backgroundColor: "white",
    marginVertical: 10,
    marginHorizontal: 5,
    borderRadius: 2,
  },
  searchIcon: {
    padding: 10,
  },
  textInput: {
    color: "black",
    width: "100%",
  },
});
