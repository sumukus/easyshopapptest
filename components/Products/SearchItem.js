import { Text, View, Image, StyleSheet, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";

function SearchItem({ product }) {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() => navigation.navigate("ProductDetails", { product })}
    >
      <View style={styles.rootContainer}>
        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={{
              uri: product.image
                ? product.image
                : "https://cdn.pixabay.com/photo/2023/03/22/21/57/animal-7870631_1280.jpg",
            }}
          />
        </View>
        <View style={styles.productDetails}>
          <Text style={styles.productName}>{product.name}</Text>
          <Text>{product.description}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

export default SearchItem;

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    paddingHorizontal: 10,
    flexDirection: "row",
    marginBottom: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  image: {
    width: "100%",
    height: "100%",
  },
  imageContainer: {
    height: 60,
    width: 60,
    flex: 1,
    borderRadius: 10,
    overflow: "hidden",
  },
  productDetails: {
    marginLeft: 10,
    borderBottomWidth: 1,
    borderColor: "grey",
    flex: 3,
  },
  productName: {
    fontSize: 14,
    fontWeight: "bold",
  },
});
