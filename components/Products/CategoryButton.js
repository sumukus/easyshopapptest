import { Pressable, Text, StyleSheet } from "react-native";

function CategoryButton({ children, onPress, style }) {
  return (
    <Pressable
      onPress={onPress}
      style={({ pressed }) =>
        pressed
          ? { ...styles.buttonContainer, ...styles.pressed, ...style }
          : { ...styles.buttonContainer, ...style }
      }
    >
      <Text style={styles.text}>{children}</Text>
    </Pressable>
  );
}

export default CategoryButton;

const styles = StyleSheet.create({
  pressed: {
    opacity: 0.75,
  },
  buttonContainer: {
    backgroundColor: "blue",
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginRight: 8,
    borderRadius: 15,
    justifyContent: "center",
    alignItems: "center",
    elevation: 5,
    // overflow: "hidden",
  },
  text: {
    color: "white",
    textAlign: "center",
  },
});
