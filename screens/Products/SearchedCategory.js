import { ScrollView, View, StyleSheet } from "react-native";
import CategoryButton from "../../components/Products/CategoryButton";

function SearchedCategory({
  categoryList,
  active,
  setActive,
  products,
  setCategoryProducts,
}) {
  return (
    <ScrollView horizontal={true}>
      <View style={styles.rootContainer}>
        <CategoryButton
          active={active}
          onPress={() => {
            setActive(-1);
            setCategoryProducts(products);
          }}
          style={active === -1 ? styles.activeColor : null}
        >
          All
        </CategoryButton>

        {categoryList.map((category) => (
          <CategoryButton
            key={category._id.$oid}
            onPress={() => {
              setActive(categoryList.indexOf(category));
              setCategoryProducts(
                products.filter(
                  (product) => product.category.$oid === category._id.$oid
                )
              );
            }}
            style={
              active === categoryList.indexOf(category)
                ? styles.activeColor
                : null
            }
          >
            {category.name}
          </CategoryButton>
        ))}
      </View>
    </ScrollView>
  );
}

export default SearchedCategory;

const styles = StyleSheet.create({
  rootContainer: {
    flexDirection: "row",
    width: "100%",
    backgroundColor: "#f2f2f2",
    paddingVertical: 7,
    paddingLeft: 10,
  },
  activeColor: {
    backgroundColor: "red",
  },
});
