import { View, Text, FlatList, StyleSheet, Dimensions } from "react-native";
import { useState, useEffect, useLayoutEffect } from "react";
import ProductList from "./ProductList";
import SearchBox from "../../components/SearchBox";
import SearchedProducts from "./SearchedProducts";
import Banner from "../../Shared/Banner";
import SearchedCategory from "./SearchedCategory";

const data = require("../../assets/data/products.json");
const category = require("../../assets/data/categories.json");

function ProductContainer() {
  const [products, setProducts] = useState([]); // store all products data
  const [searchedProducts, setSearchedProducts] = useState([]); //store the related products based on query
  const [focus, setFocus] = useState(false); // if nothing is there in search box display default product container
  const [categoryList, setCategoryList] = useState([]); //store different category name
  const [active, setActive] = useState(-1); //set color for active category
  const [categoryProducts, setCategoryProducts] = useState([]); // store the data related to specific category

  useEffect(() => {
    setProducts(data);
    setCategoryList(category);
    setCategoryProducts(data);
    return () => {
      setProducts([]);
      setCategoryList([]);
      setCategoryProducts([]);
    };
  }, []);

  function handleSearchedProducts(text) {
    if (text.length > 0) {
      setFocus(true);
      setSearchedProducts(
        products.filter((product) =>
          product.name.toLowerCase().includes(text.toLowerCase())
        )
      );
    } else {
      setFocus(false);
    }
  }
  return (
    <>
      <View style={styles.searchBox}>
        <SearchBox onChangeText={handleSearchedProducts} />
      </View>

      {focus ? (
        <SearchedProducts products={searchedProducts} />
      ) : (
        <View style={styles.rootContainer}>
          {/* <Text style={styles.title}>Product Container</Text> */}
          <FlatList
            ListHeaderComponent={() => (
              <>
                <View style={styles.headerList}>
                  <Banner />
                </View>
                <View style={styles.headerList}>
                  <SearchedCategory
                    setCategoryProducts={setCategoryProducts}
                    products={products}
                    categoryList={categoryList}
                    active={active}
                    setActive={setActive}
                  />
                </View>
              </>
            )}
            ListFooterComponent={() =>
              categoryProducts.length === 0 ? (
                <Text style={styles.footerStyle}>Products Not Available</Text>
              ) : null
            }
            ListHeaderComponentStyle={{
              // paddingVertical: 5,
              // paddingHorizontal: 10,
              // backgroundColor: "white",
              paddingTop: 10,
            }}
            numColumns={2}
            horizontal={false}
            data={categoryProducts}
            renderItem={({ item }) => <ProductList item={item} />}
            keyExtractor={(item) => item._id.$oid}
            contentContainerStyle={{ paddingBottom: 60 }}
          />
        </View>
      )}
    </>
  );
}

export default ProductContainer;

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    backgroundColor: "gainsboro",
  },
  headerList: {
    paddingBottom: 10,
    marginBottom: 10,
  },

  footerStyle: {
    fontSize: 18,
    textAlign: "center",
    fontWeight: "bold",
    paddingTop: 20,
  },
  searchBox: {
    backgroundColor: "#0353a2",
  },
});
