import { Button } from "react-native";
import { View, Text, StyleSheet, Dimensions, Image } from "react-native";
import { useDispatch } from "react-redux";
import { addToCart } from "../../store/cartSlice";

const { width } = Dimensions.get("window");

function ProductCard({ product }) {
  const dispatch = useDispatch();

  return (
    <View style={styles.rootConainer}>
      <View style={styles.imageContainer}>
        <Image
          style={styles.image}
          source={{
            uri: product.image
              ? product.image
              : "https://cdn.pixabay.com/photo/2023/03/22/21/57/animal-7870631_1280.jpg",
          }}
        />
      </View>

      <Text style={styles.title}>{product.name}</Text>
      <Text style={styles.price}>Nu.{product.price.toFixed(2)}</Text>
      {product.countInStock > 0 ? (
        <View style={styles.button}>
          <Button
            title="Add"
            color="green"
            onPress={() =>
              dispatch(addToCart({ cartOrderId: Date(), product }))
            }
          />
        </View>
      ) : (
        <Text style={styles.productStatus}>Currently Not Available</Text>
      )}
    </View>
  );
}

export default ProductCard;

const styles = StyleSheet.create({
  rootConainer: {
    width: width / 2 - 20,
    height: width / 2,
    padding: 10,
    marginTop: 60,
    marginHorizontal: 10,
    alignItems: "center",
    justifyContent: "flex-end",
    elevation: 8,
    backgroundColor: "white",
    borderRadius: 10,
  },
  title: {
    textAlign: "center",
    fontSize: 12, // default value of 14
    fontWeight: "bold",
  },
  price: {
    textAlign: "center",
    fontSize: 16,
    color: "orange",
  },
  imageContainer: {
    width: width / 2 - 20 - 10,
    height: width / 2 - 20 - 40,
    top: -45,
    borderRadius: 10,
    overflow: "hidden",
    position: "absolute",
  },
  image: {
    width: "100%",
    height: "100%",
  },
  button: {
    marginTop: 5,
  },
  productStatus: {
    color: "red",
    paddingBottom: 20,
  },
});
