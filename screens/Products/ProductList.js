import { TouchableOpacity, StyleSheet, View, Dimensions } from "react-native";
import ProductCard from "./ProductCard";
import { useNavigation } from "@react-navigation/native";

function ProductList({ item }) {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      style={styles.rootContainer}
      onPress={() => navigation.navigate("ProductDetails", { product: item })}
    >
      <View style={styles.body}>
        <ProductCard product={item} />
      </View>
    </TouchableOpacity>
  );
}
export default ProductList;

const { width } = Dimensions.get("window");

const styles = StyleSheet.create({
  rootContainer: {
    width: "50%",
  },
  body: {
    width: width / 2,
  },
});
