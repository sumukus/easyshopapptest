import { View, StyleSheet, FlatList, Text } from "react-native";

import SearchItem from "../../components/Products/SearchItem";

function SearchedProducts({ products }) {
  return (
    <View style={styles.rootContainer}>
      {products.length > 0 ? (
        <FlatList
          data={products}
          renderItem={({ item }) => <SearchItem product={item} />}
          keyExtractor={(item) => item._id.$oid}
        />
      ) : (
        <View style={styles.productNotFound}>
          <Text style={styles.text}>Product Not Found</Text>
        </View>
      )}
    </View>
  );
}

export default SearchedProducts;

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    backgroundColor: "white",
    paddingTop: 10,
  },
  productNotFound: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    fontSize: 14,
    fontWeight: "bold",
  },
});
