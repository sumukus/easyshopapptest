import { Text, View, StyleSheet, ScrollView, Button } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import SingleItem from "../../components/Cart/SingleItem";
import { Ionicons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";
import { clearCart } from "../../store/cartSlice";

function Cart() {
  const dispatch = useDispatch();
  const cartItems = useSelector((state) => state.cart.value);
  const navigation = useNavigation();
  let total = 0;
  cartItems.map((item) => (total += item.product.price));
  return (
    <View style={styles.rootContainer}>
      <ScrollView>
        {cartItems.length > 0 ? (
          cartItems.map((cartItem, index) => (
            <SingleItem key={index} cartItem={cartItem} />
          ))
        ) : (
          <>
            <Text style={styles.emptyCartText}>
              Nothing is here in Cart. Try adding some
            </Text>
            <View style={styles.addIcon}>
              <Ionicons
                name="add-circle"
                size={32}
                color="#5299df"
                onPress={() => navigation.navigate("Home")}
              />
            </View>
          </>
        )}
      </ScrollView>
      {cartItems.length > 0 ? (
        <View style={styles.cartFooter}>
          <Text style={styles.totalPrice}>Total: Nu.{total.toFixed(2)}</Text>
          <Button
            title="Clear"
            onPress={() => {
              dispatch(clearCart());
            }}
          />
          <Button
            title="Checkout"
            onPress={() => navigation.navigate("Checkout")}
          />
        </View>
      ) : null}
    </View>
  );
}

export default Cart;

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    paddingTop: 10,
  },
  emptyCartText: {
    paddingTop: 10,
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center",
  },
  addIcon: {
    flex: 1,
    alignItems: "center",
  },
  cartFooter: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingBottom: 10,
    paddingHorizontal: 10,
  },
  totalPrice: {
    color: "orange",
    fontSize: 16,
    fontWeight: "bold",
  },
});
