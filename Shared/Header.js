import { View, Image, StyleSheet } from "react-native";

function Header() {
  return (
    <View style={styles.header}>
      <Image
        source={require("../assets/Logo.png")}
        style={styles.image}
        resizeMode="contain"
      />
    </View>
  );
}

export default Header;

const styles = StyleSheet.create({
  header: {
    width: "100%",
    height: 50,
    alignContent: "center",
    alignItems: "center",
    backgroundColor: "white",
    paddingVertical: 2,
  },
  image: {
    width: "100%",
    height: "100%",
  },
});
