import { Image, View, StyleSheet, ScrollView } from "react-native";
import { useEffect, useState } from "react";
import Swiper from "react-native-swiper";
import { Dimensions } from "react-native";

const { height } = Dimensions.get("window");
function Banner() {
  const [bannerImage, setBannerImage] = useState([]);
  useEffect(() => {
    setBannerImage([
      "https://images.vexels.com/media/users/3/126443/preview2/ff9af1e1edfa2c4a46c43b0c2040ce52-macbook-pro-touch-bar-banner.jpg",
      "https://pbs.twimg.com/media/D7P_yLdX4AAvJWO.jpg",
      "https://www.yardproduct.com/blog/wp-content/uploads/2016/01/gardening-banner.jpg",
    ]);
    return () => {
      setBannerImage([]);
    };
  }, []);
  return (
    <View style={styles.rootContainer}>
      <Swiper
        showsButtons={true}
        autoplay={true}
        autoplayTimeout={3}
        buttonWrapperStyle={{
          paddingHorizontal: 0,
          paddingVertical: 0,
        }}
      >
        {bannerImage.map((image) => (
          <View style={styles.imageContainer} key={image}>
            <Image style={styles.image} source={{ uri: image }} />
          </View>
        ))}
      </Swiper>
    </View>
  );
}
export default Banner;

const styles = StyleSheet.create({
  rootContainer: {
    width: "100%",
    height: height / 4,
    borderRadius: 10,
    overflow: "hidden",
  },
  image: {
    width: "100%",
    height: "100%",
    borderRadius: 10,
    overflow: "hidden",
  },
  imageContainer: {
    paddingHorizontal: 15,
  },
});
