import { StatusBar } from "expo-status-bar";
import { StyleSheet } from "react-native";
import Header from "./Shared/Header";
import { SafeAreaView } from "react-native-safe-area-context";
// import { SafeAreaView } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import MainNavigator from "./Navigators/MainNavigator";
import { Provider } from "react-redux";
import { store } from "./store/store";

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar style="light" />
      <Header />
      <Provider store={store}>
        <NavigationContainer>
          <MainNavigator />
        </NavigationContainer>
      </Provider>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#0353a2",
  },
});
